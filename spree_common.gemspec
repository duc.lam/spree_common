# encoding: UTF-8
lib = File.expand_path('../lib/', __FILE__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'spree_common/version'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_common'
  s.version     = SpreeCommon.version
  s.summary     = 'Common code for projects using Spree 4.x+'
  s.description = 'Projects: Natures Flavors'
  s.required_ruby_version = '>= 3.0.1'

  s.author    = 'Duc T. Lam'
  s.email     = 'dlam@naturesflavors.coms'
  s.homepage  = 'https://gitlab.com/duc.lam/spree_common'
  s.license = 'BSD-3-Clause'

  s.files       = `git ls-files`.split("\n").reject { |f| f.match(/^spec/) && !f.match(/^spec\/fixtures/) }
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree', '>= 4.3'
  # s.add_dependency 'spree_backend' # uncomment to include Admin Panel changes
  s.add_dependency 'spree_extension'

  s.add_development_dependency 'spree_dev_tools'
end
