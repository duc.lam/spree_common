Rails.application.config.spree.calculators.shipping_methods << Spree::Calculator::Shipping::FreeShippingOverAmountCalculator
Rails.application.config.spree.calculators.shipping_methods << Spree::Calculator::Shipping::FreeShippingUnderAmountCalculator
Rails.application.config.spree.calculators.shipping_methods << Spree::Calculator::Shipping::LocalPickupCalculator
Rails.application.config.spree.calculators.shipping_methods << Spree::Calculator::Shipping::CustomShippingCalculator
