Spree::Core::Engine.routes.draw do
  # Override for Production state
  namespace :admin, path: Spree.admin_path do
    resource :banner_message_settings, only: [:edit, :update] do
    end
  end
end

Spree::Core::Engine.routes.draw do
  get '/category/*id/(page/:page)/(per_page/:per_page)/(sort_by/:sort_by)', :to => 'taxons#show', :as => :categories
end