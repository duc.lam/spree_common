Spree::Stock::Estimator.class_eval do

  def choose_default_shipping_rate(shipping_rates)
    unless shipping_rates.empty?
      if shipping_rates.count == 1
        shipping_rates[0].selected = true
        return shipping_rates
      end

      remaining_rates = shipping_rates.reject { |rate| rate.shipping_method.calculator.class.name == "Spree::Calculator::Shipping::LocalPickupCalculator" }
      
      if remaining_rates.nil? || remaining_rates.min_by(&:cost).nil?
        remaining_rates
      else
        remaining_rates.min_by(&:cost).selected = true
      end
    end
  end
  
end
