require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class CustomShippingCalculator < ShippingCalculator
      
      UPS_MAX_WEIGHT = 150  # 150 lbs

      def available?(package)
        package.weight >= UPS_MAX_WEIGHT
      end

      def self.description
        Spree.t(:custom_shipping)
      end

      def compute_package(package)
        0.00
      end
    end
  end
end