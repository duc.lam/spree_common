require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class FreeShippingUnderAmountCalculator < ShippingCalculator
      preference :amount, :decimal, default: 0
      preference :start_day_of_week, :string, default: ''
      preference :end_day_of_week, :string, default: ''
      preference :start_time, :string, default: ''
      preference :end_time, :string, default: ''
      
      def available?(package)
        package.order.total < preferred_amount && cover_timeframe?
      end

      def cover_timeframe?
        if preferred_start_day_of_week.blank? || preferred_end_day_of_week.blank? ||
            preferred_start_time.blank? || preferred_end_time.blank?
          return true
        end
         
        current_day_index = Date.today.wday
        start_wday = Date._strptime(preferred_start_day_of_week, '%A')[:wday]
        end_wday   = Date._strptime(preferred_end_day_of_week, '%A')[:wday]
        
        start_time = Time.parse(preferred_start_time)
        end_time   =  Time.parse(preferred_end_time)
        current_time = Time.now

        if (start_wday..end_wday).cover?(current_day_index) ||
            (start_wday..(end_wday + 7)).cover?(current_day_index)
          result = true

          if start_wday == end_wday
            result = start_time < current_time && end_time > current_time
          elsif start_wday == current_day_index
            result = start_time < current_time
          elsif end_wday == current_day_index
            result = end_time > current_time
          end

          return result
        else
          return false
        end
      end

      def self.description
        Spree.t(:free_shipping_under_amount)
      end

      def compute_package(package)
        0.00
      end
    end
  end
end