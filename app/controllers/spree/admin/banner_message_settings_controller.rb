module Spree
  module Admin
    class BannerMessageSettingsController < Spree::Admin::BaseController
      
      def update
        params.each do |name, value|
          next unless Spree::Config.has_preference? name
          Spree::Config[name] = value
        end

        flash[:success] = Spree.t(:successfully_updated, resource: Spree.t(:banner_message_settings))
        redirect_to edit_admin_banner_message_settings_url
      end
      
    end
  end
end
