Deface::Override.new(
  virtual_path: 'spree/admin/orders/edit',
  name: 'add_production_state_flow_to_orders_edit',
  insert_after: "[data-hook='admin_order_edit_header']",
  partial: "spree/admin/orders/production_state_flow"
)
