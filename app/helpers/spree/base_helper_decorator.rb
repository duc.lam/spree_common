Spree::BaseHelper.module_eval do

    def seo_url(taxon, params = nil)
      categories_path(taxon.permalink, params)
    end

    def sort_shipping_rates_local_pickup_last(shipping_rates)
      shipping_rates.sort_by {|x, y| if x.shipping_method.calculator.class.name == "Spree::Calculator::Shipping::LocalPickupCalculator" then 1 else 0 end }
    end
      
end
